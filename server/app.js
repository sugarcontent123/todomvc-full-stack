// app.js
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

// initialize express app
const route = require('./routes/route');
const app = express();

// Set up mongoose connection
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/todolist');
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", function(callback){
  console.log("Connection Succeeded");
});

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/api', route);

let port = 8081;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});