const express = require('express');
const router = express.Router();

// Require the controllers
const controller = require('../controllers/controller');

// a simple test url to check that all of our files are communicating correctly.
router.get('/test', controller.test);

// create a product
router.post('/create', controller.createItem);

// get a product by id
router.get('/:id', controller.getItem);

// get all products
router.get('/tasks', controller.getAllItems)

// update a product by id
router.put('/update', controller.updateItem);

// delete a product by id
router.delete('/delete', controller.deleteItem);

module.exports = router;