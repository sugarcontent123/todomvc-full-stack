// database
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ToDoSchema = new Schema({
    id: { type: Number, require: true },
    title: { type: String, required: true },
    completed: { type: Boolean, required: true, default: false }
});

// Export the model
module.exports = mongoose.model('ToDo', ToDoSchema, 'todos');
