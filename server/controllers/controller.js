const ToDo = require('../models/model');

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.status(200).send('Greetings from the Test controller!');
};

// create a new task
exports.createItem = function (req, res) {
    let toDoItem = new ToDo(
        {
            id: req.body.id,
            title: req.body.title,
            completed: req.body.completed
        }
    );

    toDoItem.save(function (err) {
        if (err) {
            return next(err);
        }
        res.status(200).send('Task created successfully')
    })
};

// get a task by id
exports.getItem = function (req, res) {
    ToDo.findById(req.body.id, function (err, toDoItem) {
        if (err) return next(err);
        res.send(toDoItem);
    })
};

// get all tasks
exports.getAllItems = function (req, res) {
    ToDo.find({}, function(err, toDoList) {
        if (err) return next(err);
        res.status(200).send(toDoList);
    })
};

// update a task by id
exports.updateItem = function (req, res) {
    ToDo.findOneAndUpdate({ id: req.body.id }, {$set: req.body}, function (err, toDoItem) {
        if (err) return next(err);
        res.status(200).send('Task updated.');
    });
};

// delete a single task by id
exports.deleteItem = function (req, res) {
    ToDo.findOneAndDelete({ id: req.body.id }, function (err) {
        if (err) return next(err);
        res.status(200).send('Deleted successfully!');
    })
};