/*jshint unused:false */

(function (exports) {

	'use strict';

	var STORAGE_KEY = 'wtf';

	exports.todoStorage = {
		fetch: function () {
			return JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]');
		},
		save: function (todos) {
			localStorage.setItem(STORAGE_KEY, JSON.stringify(todos));
		}
	};

	exports.operations = {
		/* Single todo item operations */
		createTask: function(todo) {
			axios.post('http://localhost:8081/api/create', todo)
				.then(function (res) {
					console.log(res.data);
					return res.data;
				})
				.catch(function (err) {
					console.log(err);
				})
		},

		updateTask: function(todo) {
			axios.put('http://localhost:8081/api/update', todo)
				.then(function (res) {
					console.log(res.data);
				})
				.catch(function (err) {
					console.log(err);
				})
		},

		deleteTask: function(todo) {
			axios.delete('http://localhost:8081/api/delete', todo)
				.then(function (res) {
					console.log(res.data);
				})
				.catch(function (err) {
					console.log(err);
				})
		},

		/* Multiple todo item operations */
		getAllTask: function() {
			axios.get('http://localhost:8081/api/tasks')
				.then(function (res) {
					console.log(res.data);
					return res.data;
				})
				.catch(function (err) {
					console.log(err);
				})
		},

		test: function() {
			axios.get('http://localhost:8081/api/test')
				.then(function (res) {
					console.log(res.data);
					return res.data;
				})
				.catch(function (err) {
					console.log(err);
				})
		},
	};

})(window);